/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

// API ENDPOINT category
Route.get('/api/categories', 'CategoriesController.index')
Route.post('/api/category/create', 'CategoriesController.store')
Route.put('/api/category/edit/:id', 'CategoriesController.update')
Route.get('/api/category/:id', 'CategoriesController.show')

// API ENDPOINT anime
Route.get('/api/animes', 'AnimeController.index')
Route.post('/api/anime/create', 'AnimeController.store')
Route.put('/api/anime/edit/:id', 'AnimeController.update')
Route.get('/api/anime/:id', 'AnimeController.show')

// API ENDPOINT tag
Route.get('/api/tags', 'TagsController.index')
Route.post('/api/tag/create', 'TagsController.store')
Route.put('/api/tag/edit/:id', 'TagsController.update')
Route.get('/api/tag/:id', 'TagsController.show')
