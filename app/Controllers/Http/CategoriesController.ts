import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Category from 'App/Models/Category'
import CategoryValidator from "App/Validators/CategoryValidator";

export default class CategoriesController {

  /**
   * @param {HttpContextContract} response
   */
  public async index({response}: HttpContextContract) {
    const categories = await Category.all()
    return response.status(200).json(categories)
  }

  /**
   * @param {HttpContextContract} request
   * @param {HttpContextContract} response
   */
  public async store({request, response}: HttpContextContract) {
    try {
      const categoryData = await request.validate(CategoryValidator)
      // Check if the field slug is valid
      if (!categoryData.slug.match('^[a-z](-?[a-z])*$')) {
        return response.status(422).json({message: 'The slug is invalid !'})
      }
      // Create new category
      const category = new Category()
      category.name = categoryData.name
      category.slug = categoryData.slug
      category.content = categoryData.content
      await category.save()
    } catch (e) {
      return response.status(422).json(e.messages)
    }

    return response.status(200).json({message: 'You has been create your category with successful !'})
  }

  /**
   * @param {HttpContextContract} request
   * @param {HttpContextContract} response
   */
  public async update({request, response}: HttpContextContract) {
    try {
      const categoryData = await request.validate(CategoryValidator)
      // Check if the field slug is valid
      if (!categoryData.slug.match('^[a-z](-?[a-z])*$')) {
        return response.status(422).json({message: 'The slug is invalid !'})
      }
      // Update to category current by id
      const category = await Category.find(request.param('id'))
      if (!category) {
        return response.status(422).json({message: 'This anime don\'t exist in our database !'})
      }
      category.name = categoryData.name
      category.slug = categoryData.slug
      category.content = categoryData.content
      await category.save()
    } catch (e) {
      return response.status(422).json(e.messages)
    }

    return response.status(200).json({message: 'You has been update your category with successful !'})
  }

  /**
   * @param {HttpContextContract} request
   * @param {HttpContextContract} response
   */
  public async show({request, response}: HttpContextContract) {
    const category = await Category.find(request.param('id'))
    if (!category) {
      return response.status(422).json({message: 'This category don\'t exist in our database !'})
    }
    return response.status(200).json(category)
  }

}
