// import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import Tag from 'App/Models/Tag'
import TagValidator from "App/Validators/TagValidator";

export default class TagsController {

  /**
   * @param {HttpContextContract} response
   */
  public async index({response}: HttpContextContract) {
    const tags = await Tag.query()
    return response.status(200).json(tags)
  }

  /**
   * @param {HttpContextContract} request
   * @param {HttpContextContract} response
   */
  public async store({request, response}: HttpContextContract) {
    try {
      const tagData = await request.validate(TagValidator)
      // Check if the field slug is valid
      if (!tagData.slug.match('^[a-z](-?[a-z])*$')) {
        return response.status(422).json({message: 'The slug is invalid !'})
      }
      // Create new tag
      const tag = new Tag()
      tag.name = tagData.name
      tag.slug = tagData.slug

      await tag.save()
    } catch (e) {
      return response.status(422).json(e.messages)
    }
    return response.status(200).json({message: 'You has been create your tag with successful !'})
  }

}
