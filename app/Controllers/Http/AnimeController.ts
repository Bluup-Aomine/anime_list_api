import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Anime from 'App/Models/Anime';
import AnimeValidator from "App/Validators/AnimeValidator";
import Category from "App/Models/Category";

export default class AnimeController {

  /**
   * @param {HttpContextContract} response
   */
  public async index({response}: HttpContextContract) {
    const animes = await Anime.query().preload('category')
    return response.status(200).json(animes)
  }

  /**
   * @param {HttpContextContract} request
   * @param {HttpContextContract} response
   */
  public async store({request, response}: HttpContextContract) {
    try {
      const animeData = await request.validate(AnimeValidator)
      // Check if the field slug is valid
      if (!animeData.slug.match('^[a-z](-?[a-z])*$')) {
        return response.status(422).json({message: 'The slug is invalid !'})
      }
      // Create new anime
      const anime = new Anime()
      anime.title = animeData.title
      anime.slug = animeData.slug
      anime.content = animeData.content
      anime.type = animeData.type
      anime.studios = animeData.studios
      anime.start_date = animeData.start_date
      anime.end_date = animeData.end_date
      anime.status = animeData.status

      // Category associate
      const category = await Category.findBy('slug', animeData.category)
      if (category) await anime.related('category').associate(category)

      await anime.save()
    } catch (e) {
      return response.status(422).json(e.messages)
    }
    return response.status(200).json({message: 'You has been create your anime with successful !'})
  }

  /**
   * @param {HttpContextContract} request
   * @param {HttpContextContract} response
   */
  public async update({request, response}: HttpContextContract)
  {
    try {
      const animeData = await request.validate(AnimeValidator)
      // Check if the field slug is valid
      if (!animeData.slug.match('^[a-z](-?[a-z])*$')) {
        return response.status(422).json({message: 'The slug is invalid !'})
      }
      // Create new anime
      const anime = await Anime.find(request.param('id'))
      if (!anime) {
        return response.status(422).json({message: 'This anime don\'t exist in our database !'})
      }
      anime.title = animeData.title
      anime.slug = animeData.slug
      anime.content = animeData.content
      anime.type = animeData.type
      anime.studios = animeData.studios
      anime.start_date = animeData.start_date
      anime.end_date = animeData.end_date
      anime.status = animeData.status

      // Category associate
      const category = await Category.findBy('slug', animeData.category)
      if (category) await anime.related('category').associate(category)

      await anime.save()
    } catch (e) {
      return response.status(422).json(e.messages)
    }
    return response.status(200).json({message: 'You has been update your anime with successful !'})
  }

  /**
   * @param {HttpContextContract} request
   * @param {HttpContextContract} response
   */
  public async show({request, response}: HttpContextContract)
  {
    const anime = await Anime.find(request.param('id'))
    if (!anime) {
      return response.status(422).json({message: 'This anime don\'t exist in our database !'})
    }
    return response.status(200).json(anime)
  }
}
