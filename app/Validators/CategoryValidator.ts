import {rules, schema} from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class CategoryValidator {
  constructor (protected ctx: HttpContextContract) {
  }

	/*
	 * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
	 *
	 * For example:
	 * 1. The username must be of data type string. But then also, it should
	 *    not contain special characters or numbers.
	 *    ```
	 *     schema.string({}, [ rules.alpha() ])
	 *    ```
	 *
	 * 2. The email must be of data type string, formatted as a valid
	 *    email. But also, not used by any other user.
	 *    ```
	 *     schema.string({}, [
	 *       rules.email(),
	 *       rules.unique({ table: 'users', column: 'email' }),
	 *     ])
	 *    ```
	 */
  public schema = schema.create({
    name: schema.string({ trim: true, escape: true }, [
      rules.required(),
      rules.minLength(5),
      rules.maxLength(255)
    ]),
    slug: schema.string({ trim: true, escape: true }, [
      rules.required(),
      rules.minLength(5),
      rules.maxLength(255)
    ]),
    content: schema.string({}, [
      rules.required(),
      rules.minLength(10)
    ])
  })

	/**
	 * Custom messages for validation failures. You can make use of dot notation `(.)`
	 * for targeting nested fields and array expressions `(*)` for targeting all
	 * children of an array. For example:
	 *
	 * {
	 *   'profile.username.required': 'Username is required',
	 *   'scores.*.number': 'Define scores as valid numbers'
	 * }
	 *
	 */
  public messages = {
    'name.required': 'The field name must be required',
    'name.minLength': 'The field name must be at lest 5 characters',
    'name.maxLength': 'The field name don\'t must be at more 255 characters',

    'slug.required': 'The field slug must be required',
    'slug.minLength': 'The field slug must be at lest 5 characters',
    'slug.maxLength': 'The field slug don\'t must be at more 255 characters',

    'content.required': 'The field content must be required',
    'content.minLength': 'The field content must be at lest 10 characters'
  }
}
