import {rules, schema} from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class CategoryValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  /*
   * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
   *
   * For example:
   * 1. The username must be of data type string. But then also, it should
   *    not contain special characters or numbers.
   *    ```
   *     schema.string({}, [ rules.alpha() ])
   *    ```
   *
   * 2. The email must be of data type string, formatted as a valid
   *    email. But also, not used by any other user.
   *    ```
   *     schema.string({}, [
   *       rules.email(),
   *       rules.unique({ table: 'users', column: 'email' }),
   *     ])
   *    ```
   */
  public schema = schema.create({
    title: schema.string({ trim: true, escape: true }, [
      rules.required(),
      rules.minLength(5),
      rules.maxLength(255)
    ]),
    slug: schema.string({ trim: true, escape: true }, [
      rules.required(),
      rules.minLength(5),
      rules.maxLength(255)
    ]),
    content: schema.string({}, [
      rules.required(),
      rules.minLength(10)
    ]),
    type: schema.string({}, [
      rules.required(),
      rules.minLength(5),
      rules.maxLength(60)
    ]),
    studios: schema.string({}, [
      rules.required(),
      rules.minLength(5),
      rules.maxLength(60)
    ]),
    start_date: schema.date({}, [
      rules.required()
    ]),
    end_date: schema.date( {}, [
      rules.required()
    ]),
    status: schema.string({trim: true, escape: true}, [
      rules.required()
    ]),
    category: schema.string({}, [rules.exists({ table: 'categories', column: 'slug' })])
  })

  /**
   * Custom messages for validation failures. You can make use of dot notation `(.)`
   * for targeting nested fields and array expressions `(*)` for targeting all
   * children of an array. For example:
   *
   * {
   *   'profile.username.required': 'Username is required',
   *   'scores.*.number': 'Define scores as valid numbers'
   * }
   *
   */
  public messages = {
    'title.required': 'The field title must be required',
    'title.minLength': 'The field title must be at lest 5 characters',
    'title.maxLength': 'The field title don\'t must be at more 255 characters',

    'slug.required': 'The field slug must be required',
    'slug.minLength': 'The field slug must be at lest 5 characters',
    'slug.maxLength': 'The field slug don\'t must be at more 255 characters',

    'content.required': 'The field content must be required',
    'content.minLength': 'The field content must be at lest 10 characters',

    'type.required': 'The field type must be required',
    'type.minLength': 'The field type must be at lest 5 characters',
    'type.maxLength': 'The field type must be at lest 60 characters',

    'studios.required': 'The field studios must be required',
    'studios.minLength': 'The field studios must be at lest 5 characters',
    'studios.maxLength': 'The field studios must be at lest 60 characters',

    'start_date.required': 'The field start_date must be required',
    'start_date.date.format' : 'The field start_date must be to type datetime',

    'end_date.required': 'The field end_date must be required',
    'end_date.date.format' : 'The field end_date must be to type datetime',

    'status.required': 'The field status must be required',

    'category.exists': 'This category selected don\'t existing in our database !'
  }
}
