import { DateTime } from 'luxon'
import {BaseModel, column, BelongsTo, belongsTo, manyToMany} from '@ioc:Adonis/Lucid/Orm'
import Category from "App/Models/Category";
import {ManyToMany} from "@ioc:Adonis/Lucid/Relations";
import Tag from "App/Models/Tag";

export default class Anime extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({})
  public title: string

  @column({})
  public slug: string

  @column({})
  public content: string

  @column({})
  public categoryId: number

  @belongsTo(() => Category, {
    foreignKey: 'categoryId'
  })
  public category: BelongsTo<typeof Category>

  @column({})
  public type: string

  @column({})
  public studios: string

  @column.dateTime({})
  public start_date: DateTime

  @column.dateTime({})
  public end_date: DateTime

  @column({})
  public status: string

  @manyToMany(() => Tag, {
    pivotTable: 'tag_anime'
  })
  public tags: ManyToMany<typeof Tag>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
