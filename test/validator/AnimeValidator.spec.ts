import test from 'japa'
import supertest from 'supertest'
import Database from '@ioc:Adonis/Lucid/Database'
import {ValidatorDataError} from '../type/ValidatorType'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

const getResponse = async (inputs: object | undefined = undefined) => {
  return supertest(BASE_URL)
    .post('/api/anime/create')
    .send(inputs)
    .expect(422)
}

test.group('Anime Validator', (group) => {

  group.beforeEach(async () => {
    await Database.beginGlobalTransaction()
  })

  group.afterEach(async () => {
    await Database.rollbackGlobalTransaction()
  })

  test('validate error when the field title is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'title')

    assert.equal(error && error.message, 'The field title must be required')
  })

  test('validate error when the field slug is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'slug')

    assert.equal(error && error.message, 'The field slug must be required')
  })

  test('validate error when the field content is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'content')

    assert.equal(error && error.message, 'The field content must be required')
  })

  test('validate error when the field type is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'type')

    assert.equal(error && error.message, 'The field type must be required')
  })

  test('validate error when the field studios is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'studios')

    assert.equal(error && error.message, 'The field studios must be required')
  })

  test('validate error when the field start_date is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'start_date')

    assert.equal(error && error.message, 'The field start_date must be required')
  })

  test('validate error when the field end_date is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'end_date')

    assert.equal(error && error.message, 'The field end_date must be required')
  })

  test('validate error when the field status is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'status')

    assert.equal(error && error.message, 'The field status must be required')
  })

  test('validate error when the field title is too short', async (assert) => {
    const inputs = {
      title: 'Test'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'title')

    assert.equal(error && error.message, 'The field title must be at lest 5 characters')
  })

  test('validate error when the field slug is too short', async (assert) => {
    const inputs = {
      slug: 'Test'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'slug')

    assert.equal(error && error.message, 'The field slug must be at lest 5 characters')
  })

  test('validate error when the field content is too short', async (assert) => {
    const inputs = {
      content: 'Test'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'content')

    assert.equal(error && error.message, 'The field content must be at lest 10 characters')
  })

  test('validate error when the field type is too short', async (assert) => {
    const inputs = {
      type: 'Test'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'type')

    assert.equal(error && error.message, 'The field type must be at lest 5 characters')
  })

  test('validate error when the field studios is too short', async (assert) => {
    const inputs = {
      studios: 'Test'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'studios')

    assert.equal(error && error.message, 'The field studios must be at lest 5 characters')
  })

  test('validate error when the field title is too long', async (assert) => {
    const inputs = {
      title: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'title')

    assert.equal(error && error.message, 'The field title don\'t must be at more 255 characters')
  })

  test('validate error when the field slug is too long', async (assert) => {
    const inputs = {
      slug: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'slug')

    assert.equal(error && error.message, 'The field slug don\'t must be at more 255 characters')
  })

  test('validate error when the field type is too long', async (assert) => {
    const inputs = {
      type: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'type')

    assert.equal(error && error.message, 'The field type must be at lest 60 characters')
  })

  test('validate error when the field studios is too long', async (assert) => {
    const inputs = {
      studios: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'studios')

    assert.equal(error && error.message, 'The field studios must be at lest 60 characters')
  })

  test('validate error when the field start_date format is invalid', async (assert) => {
    const inputs = {
      start_date: 'je suis invalid'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'start_date')

    assert.equal(error && error.message, 'The field start_date must be to type datetime')
  })

  test('validate error when the field end_date format is invalid', async (assert) => {
    const inputs = {
      end_date: 'je suis invalid'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'end_date')

    assert.equal(error && error.message, 'The field end_date must be to type datetime')
  })
})
