import test from 'japa'
import supertest from 'supertest'
import Database from '@ioc:Adonis/Lucid/Database'
import {ValidatorDataError} from '../type/ValidatorType'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

const getResponse = async (inputs: object | undefined = undefined) => {
  return supertest(BASE_URL)
    .post('/api/tag/create')
    .send(inputs)
    .expect(422)
}

test.group('Tag Validator', (group) => {

  group.beforeEach(async () => {
    await Database.beginGlobalTransaction()
  })

  group.afterEach(async () => {
    await Database.rollbackGlobalTransaction()
  })

  test('validate error when the field name is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'name')

    assert.equal(error && error.message, 'The field name must be required')
  })

  test('validate error when the field slug is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'slug')

    assert.equal(error && error.message, 'The field slug must be required')
  })

  test('validate error when the field name is too short', async (assert) => {
    const inputs = {
      name: 'ta'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'name')

    assert.equal(error && error.message, 'The field name must be at lest 3 characters')
  })

  test('validate error when the field slug is too short', async (assert) => {
    const inputs = {
      slug: 'ta'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'slug')

    assert.equal(error && error.message, 'The field slug must be at lest 3 characters')
  })

  test('validate error when the field name is too long', async (assert) => {
    const inputs = {
      name: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'name')

    assert.equal(error && error.message, 'The field name don\'t must be at more 60 characters')
  })

  test('validate error when the field slug is too long', async (assert) => {
    const inputs = {
      slug: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'slug')

    assert.equal(error && error.message, 'The field slug don\'t must be at more 60 characters')
  })
})
