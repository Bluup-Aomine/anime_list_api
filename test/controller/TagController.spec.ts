import test from 'japa'
import supertest from 'supertest'
import Database from '@ioc:Adonis/Lucid/Database'
// Category
import {CategoriesFactory, CategoryFactory} from "Database/factories/category";
import Category from 'App/Models/Category';

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

test.group('Tag Controller', (group) => {

  group.beforeEach(async () => {
    await Database.beginGlobalTransaction()
  })

  group.afterEach(async () => {
    await Database.rollbackGlobalTransaction()
  })

  test('index action tag controller', async (assert) => {
    await CategoriesFactory.createMany(10)

    const response = await supertest(BASE_URL).get('/api/categories').expect(200)

    assert.lengthOf(response.body, 10)
  })

  test('index action category controller with 0 result', async (assert) => {
    await CategoriesFactory.createMany(0)

    const response = await supertest(BASE_URL).get('/api/categories').expect(200)

    assert.lengthOf(response.body, 0)
  })

  test('creating new category with bad credentials', async () => {
    const inputs = {
      name: '',
      slug: '',
      content: ''
    }

    await supertest(BASE_URL)
      .post('/api/category/create')
      .send(inputs)
      .expect(422)
  })

  test('creating new category with bad slug', async (assert) => {
    const inputs = {
      name: 'Test de test',
      slug: 'Test de test',
      content: 'Test de test du test !'
    }

    const response = await supertest(BASE_URL)
      .post('/api/category/create')
      .send(inputs)
      .expect(422)

    // Message error response
    assert.equal(response.body.message, 'The slug is invalid !')
  })

  test('creating new category with good credentials', async (assert) => {
    const inputs = {
      name: 'Test de test',
      slug: 'test-de-test',
      content: 'Test de test du test !'
    }

    const response = await supertest(BASE_URL)
      .post('/api/category/create')
      .send(inputs)
      .expect(200)

    const category = await Category.query()
      .orderBy('created_at', 'desc')
      .firstOrFail()
    assert.equal(category.name, inputs.name)
    assert.equal(category.slug, inputs.slug)
    assert.equal(category.content, inputs.content)

    // Message response
    assert.equal(response.body.message, 'You has been create your category with successful !')
  })

  test('updating to category current with bad credentials', async () => {
    const inputs = {
      name: '',
      slug: '',
      content: ''
    }

    await supertest(BASE_URL)
      .put('/api/category/edit/1')
      .send(inputs)
      .expect(422)
  })

  test('updating to category current with bad slug', async (assert) => {
    const inputs = {
      name: 'Test de test',
      slug: 'Test de test',
      content: 'Test de test du test !'
    }

    const response = await supertest(BASE_URL)
      .put('/api/category/edit/1')
      .send(inputs)
      .expect(422)

    // Message error response
    assert.equal(response.body.message, 'The slug is invalid !')
  })

  test('updating to category current with bad id or don\'t exist', async (assert) => {
    const inputs = {
      name: 'Test de test',
      slug: 'test-de-test',
      content: 'Test de test du test !'
    }

    const response = await supertest(BASE_URL)
      .put('/api/category/edit/500')
      .send(inputs)
      .expect(422)

    // Message error response
    assert.equal(response.body.message, 'This anime don\'t exist in our database !')
  })

  test('updating to category current with good credentials', async (assert) => {
    await CategoryFactory.create()

    const inputs = {
      name: 'Test de test EDIT',
      slug: 'test-de-test',
      content: 'Test de test du test !'
    }

    const response = await supertest(BASE_URL)
      .put('/api/category/edit/12')
      .send(inputs)
      .expect(200)

    const category = await Category.query()
      .orderBy('created_at', 'desc')
      .firstOrFail()
    const categories = await Category.all()
    assert.equal(category.name, inputs.name)
    assert.equal(category.slug, inputs.slug)
    assert.equal(category.content, inputs.content)
    assert.lengthOf(categories, 1)

    // Message response
    assert.equal(response.body.message, 'You has been update your category with successful !')
  })

  test('show to category with bad id or don\'t exist', async (assert) => {
    const response = await supertest(BASE_URL)
      .get('/api/category/400')
      .expect(422)
    // Message response
    assert.equal(response.body.message, 'This category don\'t exist in our database !')
  })

  test('show to anime with good id', async (assert) => {
    await CategoryFactory.create()

    const response = await supertest(BASE_URL)
      .get('/api/category/13')
      .expect(200)
    const anime = await Category.findOrFail(13)
    // Message response
    assert.equal(response.body.id, anime.id)
  })

})
