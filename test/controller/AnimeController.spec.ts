import test from 'japa'
import supertest from 'supertest'
import Database from '@ioc:Adonis/Lucid/Database'
// Anime
import {AnimesFactory, AnimeFactory} from 'Database/factories/anime'
import {CategoryFactory} from 'Database/factories/category'
import Anime from 'App/Models/Anime'
import {DateTime} from 'luxon'
import Category from 'App/Models/Category'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

const CreateAssociateAnime = async () => {
  await AnimeFactory.create()
  // Associate the category to last anime
  const anime = await Anime.findByOrFail('slug', 'one-piece')
  const category = await Category.findByOrFail('slug', 'manga')
  await anime.related('category').associate(category)
}

test.group('Anime Controller', (group) => {

  group.before(async () => {
    // Create factory to model category
    await CategoryFactory.create()
    await CreateAssociateAnime()
  })

  group.beforeEach(async () => {
    await Database.beginGlobalTransaction()
  })

  group.afterEach(async () => {
    await Database.rollbackGlobalTransaction()
  })

  test('index action anime controller', async (assert) => {
    await AnimesFactory.createMany(10)

    const response = await supertest(BASE_URL).get('/api/animes').expect(200)

    assert.lengthOf(response.body, 11)
  })

  test('index action anime controller with 1 result', async (assert) => {
    const response = await supertest(BASE_URL).get('/api/animes').expect(200)

    assert.lengthOf(response.body, 1)
  })

  test('creating new anime with bad credentials', async () => {
    const inputs = {
      title: '',
      slug: '',
      content: ''
    }

    await supertest(BASE_URL)
      .post('/api/anime/create')
      .send(inputs)
      .expect(422)
  })

  test('creating new anime with bad slug', async (assert) => {
    const inputs = {
      title: 'Test de test',
      slug: 'Test de test',
      content: 'Test de test du test !',
      type: 'Type test',
      studios: 'Studios test',
      start_date: new Date('2020/05/18 15:00:00'),
      end_date: new Date('2020/07/18 17:00:00'),
      category: 'manga',
      status: 'end'
    }

    const response = await supertest(BASE_URL)
      .post('/api/anime/create')
      .send(inputs)
      .expect(422)

    // Message error response
    assert.equal(response.body.message, 'The slug is invalid !')
  })

  test('creating new anime with good credentials', async (assert) => {
    const inputs = {
      title: 'Test de test',
      slug: 'test-de-test',
      content: 'Test de test du test !',
      type: 'Type test',
      studios: 'Studios test',
      start_date: new Date('2020/05/18 15:00:00'),
      end_date: new Date('2020/07/18 17:00:00'),
      category: 'manga',
      status: 'end'
    }

    const response = await supertest(BASE_URL)
      .post('/api/anime/create')
      .send(inputs)
      .expect(200)

    const anime = await Anime.query()
      .preload('category')
      .orderBy('created_at', 'desc')
      .firstOrFail()
    assert.equal(anime.title, inputs.title)
    assert.equal(anime.slug, inputs.slug)
    assert.equal(anime.content, inputs.content)
    assert.equal(anime.type, inputs.type)
    assert.equal(anime.studios, inputs.studios)
    assert.equal(anime.start_date.toFormat('YYYY-MM-DD HH:mm:ss').toString(), DateTime.fromJSDate(inputs.start_date).toFormat('YYYY-MM-DD HH:mm:ss').toString())
    assert.equal(anime.end_date.toFormat('YYYY-MM-DD HH:mm:ss').toString(), DateTime.fromJSDate(inputs.end_date).toFormat('YYYY-MM-DD HH:mm:ss').toString())
    assert.equal(anime.category.slug, inputs.category)
    assert.equal(anime.status, inputs.status)

    // Message response
    assert.equal(response.body.message, 'You has been create your anime with successful !')
  })

  test('updating to anime current with bad credentials', async () => {
    const inputs = {
      title: '',
      slug: '',
      content: ''
    }

    await supertest(BASE_URL)
      .put(`/api/anime/edit/1`)
      .send(inputs)
      .expect(422)
  })

  test('updating to anime current with bad slug', async (assert) => {
    const inputs = {
      title: 'Test de test',
      slug: 'Test de test',
      content: 'Test de test du test !',
      type: 'Type test',
      studios: 'Studios test',
      start_date: new Date('2020/05/18 15:00:00'),
      end_date: new Date('2020/07/18 17:00:00'),
      category: 'manga',
      status: 'end'
    }

    const response = await supertest(BASE_URL)
      .put('/api/anime/edit/1')
      .send(inputs)
      .expect(422)

    // Message error response
    assert.equal(response.body.message, 'The slug is invalid !')
  })

  test('updating to anime current with bad id or don\'t exist', async (assert) => {
    const inputs = {
      title: 'Test de test',
      slug: 'test-de-test',
      content: 'Test de test du test !',
      type: 'Type test',
      studios: 'Studios test',
      start_date: new Date('2020/05/18 15:00:00'),
      end_date: new Date('2020/07/18 17:00:00'),
      category: 'manga',
      status: 'end'
    }

    const response = await supertest(BASE_URL)
      .put('/api/anime/edit/5')
      .send(inputs)
      .expect(422)

    // Message error response
    assert.equal(response.body.message, 'This anime don\'t exist in our database !')
  })

  test('updating to anime current with good credentials', async (assert) => {
    const inputs = {
      title: 'Test de test EDIT',
      slug: 'test-de-test',
      content: 'Test de test du test !',
      type: 'Type test',
      studios: 'Studios test',
      start_date: new Date('2020/05/18 15:00:00'),
      end_date: new Date('2020/07/18 17:00:00'),
      category: 'manga',
      status: 'end'
    }

    const response = await supertest(BASE_URL)
      .put('/api/anime/edit/1')
      .send(inputs)
      .expect(200)

    const anime = await Anime.query()
      .preload('category')
      .orderBy('created_at', 'desc')
      .firstOrFail()
    const animes = await Anime.all()

    assert.equal(anime.title, inputs.title)
    assert.equal(anime.slug, inputs.slug)
    assert.equal(anime.content, inputs.content)
    assert.equal(anime.type, inputs.type)
    assert.equal(anime.studios, inputs.studios)
    assert.equal(anime.start_date.toFormat('YYYY-MM-DD HH:mm:ss').toString(), DateTime.fromJSDate(inputs.start_date).toFormat('YYYY-MM-DD HH:mm:ss').toString())
    assert.equal(anime.end_date.toFormat('YYYY-MM-DD HH:mm:ss').toString(), DateTime.fromJSDate(inputs.end_date).toFormat('YYYY-MM-DD HH:mm:ss').toString())
    assert.equal(anime.category.slug, inputs.category)
    assert.equal(anime.status, inputs.status)
    assert.lengthOf(animes, 1)

    // Message response
    assert.equal(response.body.message, 'You has been update your anime with successful !')
  })

  test('show to anime with bad id or don\'t exist', async (assert) => {
    const response = await supertest(BASE_URL)
      .put('/api/anime/400')
      .expect(422)
    // Message response
    assert.equal(response.body.message, 'This anime don\'t exist in our database !')
  })

  test('show to anime with good id', async (assert) => {
    const response = await supertest(BASE_URL)
      .put('/api/anime/1')
      .expect(200)
    const anime = await Anime.findOrFail(1)
    // Message response
    assert.equal(response.body.id, anime.id)
  })

})
