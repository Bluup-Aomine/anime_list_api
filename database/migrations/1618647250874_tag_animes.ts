import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TagAnimes extends BaseSchema {
  protected tableName = 'tag_anime'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('anime_id').unsigned().references('id').inTable('anime')
      table.integer('tag_id').unsigned().references('id').inTable('tags')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
