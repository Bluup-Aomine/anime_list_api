import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Anime extends BaseSchema {
  protected tableName = 'anime'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('title').unique().notNullable()
      table.string('slug').unique().notNullable()
      table.text('content').notNullable()
      table.integer('category_id').unsigned().references('id').inTable('categories')
      table.string('type').notNullable()
      table.string('studios').notNullable()
      table.dateTime('start_date').notNullable()
      table.dateTime('end_date').notNullable()
      table.string('status').notNullable()
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
