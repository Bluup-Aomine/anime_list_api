import Factory from '@ioc:Adonis/Lucid/Factory'
import Category from "App/Models/Category";

export const CategoriesFactory = Factory.define(Category, ({faker}) => {
  return {
    name: faker.random.word(),
    slug: faker.random.word(),
    content: faker.lorem.words(5)
  }
}).build()

export const CategoryFactory = Factory.define(Category, () => {
  return {
    name: 'Manga',
    slug: 'manga',
    content: 'Description de test !'
  }
}).build()
