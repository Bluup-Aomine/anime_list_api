import Factory from '@ioc:Adonis/Lucid/Factory'
import Anime from "App/Models/Anime"
import slugify from "slugify"
import {DateTime} from "luxon";
import Category from "App/Models/Category";

export const AnimesFactory = Factory.define(Anime, ({faker}) => {
  const title = faker.random.word()

  return {
    title: faker.random.word(),
    slug: slugify(title),
    content: faker.lorem.words(5),
    type: faker.lorem.word(8),
    studios: faker.random.word(),
    start_date: DateTime.fromISO("2020-02-15T08:30:00"),
    end_date: DateTime.fromISO("2017-03-15T16:00:00"),
    status: faker.random.word()
  }
}).build()

export const AnimeFactory = Factory.define(Anime, () => {
  return {
    title: 'One Piece',
    slug: 'one-piece',
    content: 'Description sur one piece !',
    type: 'TV Series',
    studios: 'Lerche',
    start_date: DateTime.fromISO("2020-02-15T08:30:00"),
    end_date: DateTime.fromISO("2017-03-15T16:00:00"),
    status: 'progress',
    category_id: 1
  }
}).build()

